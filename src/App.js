import React, { Component } from 'react';
import { View } from 'react-native';
import firebase from 'react-native-firebase';
import { Header, Button, Spinner } from './components/common/index';
import LoginForm from './components/LoginForm';

class App extends Component {
  state = { loggedIn: null };

  componentWillMount() {
    firebase.initializeApp({
      appId: '1:846938385288:android:d82de6c38f98bfde',
      apiKey: "AIzaSyDg6VSBm2pW9NvMgHC6TAs7jgRX-FhIg0o",
      authDomain: "auth-95806.firebaseapp.com",
      databaseURL: "https://auth-95806.firebaseio.com",
      projectId: "auth-95806",
      storageBucket: "auth-95806.appspot.com",
      messagingSenderId: "846938385288"
    });

    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.setState({ loggedIn: true });
      } else {
        this.setState({ loggedIn: false });
      }
    });
  }

  renderContent() {
    switch (this.state.loggedIn) {
      case true:
        return (
          <Button onPress={() => firebase.auth().signOut()}>
            Log Out
          </Button>
        );
      case false:
        return <LoginForm />;
      default:
        return <Spinner size="large" />;
    }
  }

  render() {
    return (
      <View>
        <Header headerText="Authentication" />
        {this.renderContent()}
      </View>
    );
  }
}

export default App;
